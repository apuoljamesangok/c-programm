#include <stdio.h>
#include <stdlib.h>

struct Node
{
        int number;
        struct Node *next;
};

// Function prototypes
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

int main()
{
        struct Node *head = NULL;
        int choice, data;

        while (1)
        {
                printf("Linked Lists\n");
                printf("1. Print List\n");
                printf("2. Append\n");
                printf("3. Prepend\n");
                printf("4. Delete\n");
                printf("5. Exit\n");
                printf("Enter your choice: ");
                scanf("%d", &choice);

                switch (choice)
                {
                case 1:
                        printList(head);
                        break;
                case 2:
                        printf("Enter the value to append: ");
                        scanf("%d", &data);
                        append(&head, data);
                        break;
                case 3:
                        printf("Enter a value to prepend: ");
                        scanf("%d", &data);
                        prepend(&head, data);
                        break;
                case 4:
                        printf("Enter the value to delete: ");
                        scanf("%d", &data);
                        deleteByValue(&head, data);
                        break;
                case 5:
                        return 1;
                default:
                        printf("Invalid input.");
                        break;
                }
        }

        return 0;
}

struct Node *createNode(int num)
{
        struct Node *newNode = malloc(sizeof(struct Node));
        newNode->number = num;
        newNode->next = NULL;
        return newNode;
}

void printList(struct Node *head)
{
        struct Node *temporary = head;
        printf("[ ");
        while (temporary != NULL)
        {
                printf("%d, ", temporary->number);
                temporary = temporary->next;
        }
        printf(" ]\n");
}

void append(struct Node **head, int num)
{
        struct Node *newNode = createNode(num);
        if (*head == NULL)
        {
                *head = newNode;
                return;
        }
        struct Node *temporary = *head;
        while (temporary->next != NULL)
        {
                temporary = temporary->next;
        }
        temporary->next = newNode;
}

void prepend(struct Node **head, int num)
{
        struct Node *newNode = createNode(num);
        newNode->next = *head;
        *head = newNode;
}

void deleteByKey(struct Node **head, int key)
{
        struct Node *temporary = *head, *previous = NULL;
        if (temporary != NULL && temporary->number == key)
        {
                *head = temporary->next;
                free(temporary);
                return;
        }
        while (temporary != NULL && temporary->number != key)
        {
                previous = temporary;
                temporary = temporary->next;
        }
        if (temporary == NULL)
        {
                return;
        }
        previous->next = temporary->next;
        free(temporary);
}

void deleteByValue(struct Node **head, int value)
{
        struct Node *temporary = *head, *previous = NULL;
        if (temporary != NULL && temporary->number == value)
        {
                *head = temporary->next;
                free(temporary);
                return;
        }
        while (temporary != NULL && temporary->number != value)
        {
                previous = temporary;
                temporary = temporary->next;
        }
        if (temporary == NULL)
        {
                return;
        }
        previous->next = temporary->next;
        free(temporary);
}

void insertAfterKey(struct Node **head, int key, int value)
{
        struct Node *temporary = *head;
        while (temporary != NULL && temporary->number != key)
        {
                temporary = temporary->next;
        }
        if (temporary == NULL)
        {
                return;
        }
        struct Node *newNode = createNode(value);
        newNode->next = temporary->next;
        temporary->next = newNode;
}

void insertAfterValue(struct Node **head, int searchValue, int newValue)
{
        struct Node *temporary = *head;
        while (temporary != NULL && temporary->number != searchValue)
        {
                temporary = temporary->next;
        }
        if (temporary == NULL)
        {
                return;
        }
        struct Node *newNode = createNode(newValue);
        newNode->next = temporary->next;
        temporary->next = newNode;
}
